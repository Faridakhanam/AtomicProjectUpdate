-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 31, 2008 at 08:09 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomicproject1`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthdates`
--

CREATE TABLE IF NOT EXISTS `birthdates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `birthdate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `birthdates`
--

INSERT INTO `birthdates` (`id`, `name`, `birthdate`) VALUES
(3, '  ', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author`) VALUES
(3, ' ersf ', ' ewrerwt  '),
(4, ' ssssdfd ', ' fgfg '),
(5, ' dsrgfdg ', ' gfdfd '),
(6, '  ', '  '),
(7, '  ', '  '),
(8, '  ', '  '),
(9, '  ', '  '),
(10, '  ', '  '),
(11, '  ', '  '),
(12, '  ', '  '),
(13, '  ', '  '),
(14, '  ', '  '),
(15, '  ', '  '),
(16, '  ', '  '),
(17, '  ', '  '),
(18, '  ', '  '),
(19, '  ', '  ');

-- --------------------------------------------------------

--
-- Table structure for table `citys`
--

CREATE TABLE IF NOT EXISTS `citys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `citys`
--

INSERT INTO `citys` (`id`, `name`, `city`) VALUES
(1, 'farida', 'gopalgonj'),
(2, '', ''),
(3, '', ''),
(4, 'chaid', 'dhaka');

-- --------------------------------------------------------

--
-- Table structure for table `conditions`
--

CREATE TABLE IF NOT EXISTS `conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `condition` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `conditions`
--

INSERT INTO `conditions` (`id`, `name`, `condition`) VALUES
(2, 'fsaisfgd', ''),
(3, 'reasba', 'condition');

-- --------------------------------------------------------

--
-- Table structure for table `educations`
--

CREATE TABLE IF NOT EXISTS `educations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `education_lebel` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `educations`
--

INSERT INTO `educations` (`id`, `name`, `education_lebel`) VALUES
(1, 'farida', 'm.sc'),
(2, '', ''),
(3, 'kjhan', ''),
(4, 'chaida', ''),
(5, '', ''),
(6, 'dfgdfg', ''),
(15, 'xzczxc', ''),
(16, 'cgfdg', ''),
(17, 'fgf', '');

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE IF NOT EXISTS `emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `name`, `email`) VALUES
(9, ' gjkh ', ' jgjkgh@gmail.com '),
(10, ' dgdfg ', ' sdfsdf@gmai.com ');

-- --------------------------------------------------------

--
-- Table structure for table `summarys`
--

CREATE TABLE IF NOT EXISTS `summarys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(255) NOT NULL,
  `summary` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `summarys`
--

INSERT INTO `summarys` (`id`, `company`, `summary`) VALUES
(7, ' zxcvcxv ', '  ewrewre ');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
