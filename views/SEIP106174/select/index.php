<?php 
        include_once("../../../vendor/autoload.php");

        Use App\Bitm\SEIP106174\select\city;
        $city=new City();
        $citys=$city->index();
        
    ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <p> Select City </p>
  <body>
         <div><span> Search / Filter| <spin id="utility"> Download as PDF | XL| <a href="create.php"> Add new </a></span>
            <select class="Number">
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
                </select>
                  
         </div>
		<table class="table table-bordered">
          <thead>
			<tr>
                <th>S1.</th>
				<th>ID</th>
                <th>Name &dArr;</th>
                <th>City &dArr;</th>
				<th>Action</th>
			</tr>
          </thead>
          <tbody>
            <?php
            $slno =1;
			foreach($citys as $city){
           ?>
           <tr>
			   <td><?php echo $slno;?></td>
			   <td><?php echo $city->id;?></td>
			  <td><?php echo $city->name;?></td>
			   <td><?php echo $city->city;?></td>
			   <td>
				<a href="show.php?id=<?php echo $city->id;?>">View</a>
                <a href="edit.php?id=<?php echo $city->id;?>">Edit</a>
                <a href="delete.php?id=<?php echo $city->id;?>" class="delete">Delete</a>
                | Trash/Recover | Email to Friend
				</td>
            </tr>
               <?php
               $slno++;
                }
                ?>
            </tbody>                                                           
		</table>
        <div><span> prev  1 | 2 | 3 next </span></div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>