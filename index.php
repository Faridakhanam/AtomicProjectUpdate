<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <h1> BITM - Web App Dev - PHP </h1>
	
	<dl class="dl-horizontal">
		<dt> Name :</dt>
		<dd> Farida Khanam </dd>
		
		<dt> SEIP </dt>
		<dd> 106174 </dd>
		
		<dt> Batch </dt>
		<dd> 11 </dd>
	</dl>  
	<h2> Project Name </h2>
		<table class="table table-bordered">
		   <thead>
						<tr>
							<th> S1. </th>
							<th> Project </th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td> 01 </td>
							<td><a href="views/SEIP106174/book_title/"> Book title </a></td>
						</tr>
						<tr>
							<td> 02 </td>
							<td><a href="views/SEIP106174/chackbox_multiple/"> Chackbox Multiple </a></td>
						</tr>
						<tr>
							<td> 03 </td>
							<td><a href="views/SEIP106174/chackbox_single/"> Chackbox Single </a></td>
						</tr>
						<tr>
							<td> 04 </td>
							<td><a href="views/SEIP106174/date/"> Date </a></td>
						</tr>
						<tr>
							<td> 05 </td>
							<td><a href="views/SEIP106174/email/"> Email </a></td>
						</tr>
						<tr>
							<td> 06 </td>
							<td><a href="views/SEIP106174/file/"> File </a></td>
						</tr>
						<tr>
							<td> 07 </td>
							<td><a href="views/SEIP106174/radio/"> Radio </a></td>
						</tr>
						<tr>
							<td> 08 </td>
							<td><a href="views/SEIP106174/select/"> Select </a></td>
						</tr>
						<tr>
							<td> 09 </td>
							<td><a href="views/SEIP106174/radio/"> textarea </a></td>
						</tr>
					</tbody>
		</table>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>