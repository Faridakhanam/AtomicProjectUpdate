<?php 
        include_once("../../../vendor/autoload.php");

        Use App\Bitm\SEIP106174\select\city;
        $city=new City();
        $citys=$city->show($_GET['id']);
        
    ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <p> Select City </p>
  <h1>City details</h1>	
        
            <table class="table table-bordered">
            <thead>
			<tr>
                                                                        
				<th>ID</th>
                <th>Name &dArr;</th>
                <th>City &dArr;</th>
				
			</tr>
               </thead>
               <tbody>
                                                                
             <tr>
                                                                
              <td><?php echo $citys['id'];?></td>
              <td><?php echo $citys['name'];?></td>
              <td><?php echo $citys['city'];?></td>
                                                                     
             </tr>                                              
             </tbody> 
		</table>
         <nav>
           <li><a href="index.php">Go to list</a></li>
         </nav>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>